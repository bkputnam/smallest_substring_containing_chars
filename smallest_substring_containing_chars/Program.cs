﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace smallest_substring_containing_chars
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            String input = GetDeclarationOfIndependence();
            char[] charset = new char[] { 'a', 'b', 'c' };

            // Keep track of most recent sightings of each char in charset
            // Initialize the array with -1 to represent the fact that we
            // start off having not seen any of the chars in the charset
            int[] mostRecentSightings = new int[charset.Length];
            for (int charIndex = 0; charIndex < charset.Length; charIndex++)
            {
                mostRecentSightings[charIndex] = -1;
            }

            // Iterate through the input once. Keep track of the most recent
            // sightings of each charset char. Whenever we see a new char
            // in the input string from the charset, compute the smallest
            // substring containing all charset chars that also ends on
            // this character. If the length of this substring is less than
            // all previously seen such substrings, record the result and
            // continue iterating.
            int minLength = int.MaxValue;
            int maxSubstrStartIndex = -1;
            for (int inputIndex = 0; inputIndex < input.Length; inputIndex++)
            {
                char inputChar = input[inputIndex];

                // Iterate over every char in charset and update the
                // mostRecentSighting index. Also record whether or not inputChar
                // is in charset.
                bool inputCharIsInCharset = false;
                for (int charIndex = 0; charIndex < charset.Length; charIndex++)
                {
                    char setChar = charset[charIndex];
                    int prevMostRecentSighting = mostRecentSightings[charIndex];

                    int newSightingIndex =
                        inputChar == setChar ? 0 : // if inputChar==setChar, the most recent sighting of setChar is this char and so the index is "0 indices ago"
                        prevMostRecentSighting == -1 ? -1 : // if we haven't see setChar yet, then we still haven't seen setChar
                        (prevMostRecentSighting + 1); // otherwise the most recent sighting is now one index futher away than last time

                    inputCharIsInCharset = inputCharIsInCharset || newSightingIndex == 0;

                    mostRecentSightings[charIndex] = newSightingIndex;
                }

                // If any index in mostRecentSightings is -1 then we haven't seen all of the
                // charset chars yet, and so we cannot construct a substring that contains
                // all of them using only the previously-seen input
                bool seenAllChars = !mostRecentSightings.Any(index => index == -1);

                // Attempt to update the min substring result
                if (inputCharIsInCharset && seenAllChars)
                {
                    
                    int substringLength = mostRecentSightings.Max() + 1;
                    if( substringLength < minLength )
                    {
                        minLength = substringLength;
                        maxSubstrStartIndex = inputIndex - substringLength + 1;
                    }
                }
            }

            // Output the result, if one was found.
            if( maxSubstrStartIndex == -1 )
            {
                Console.WriteLine("Unable to find a substring that contains all charset characters");
            }
            else
            {
                String result = input.Substring(maxSubstrStartIndex, minLength);
                String prefix = input.Substring(maxSubstrStartIndex - 20, 20);
                String suffix = input.Substring(maxSubstrStartIndex + minLength, 20);
                Console.WriteLine("Result: ");
                Console.WriteLine(result);
                Console.WriteLine("Result with context:");
                Console.WriteLine(prefix + "[" + result + "]" + suffix);
            }
        }

        private static String GetDeclarationOfIndependence()
        {
            Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Stream resource = assembly.GetManifestResourceStream("DeclarationOfIndependence.txt");
            String declOfIndep = new StreamReader(resource).ReadToEnd();
            declOfIndep = declOfIndep.Replace('\n', ' ');
            return declOfIndep;
        }
    }
}
