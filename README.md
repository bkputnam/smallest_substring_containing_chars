# Smallest substring containing all chars in char set

**Problem:** Given an input string and a set of characters, find the smallest substring in the input
string that contains all of the characters in the set.

**Example:** If the input string is `"abaracadabara"` and the character set is `['a', 'b', 'c']`, the
smallest substring containing all 3 characters is either `"barac"` or `"cadab"`

Source (problem #1): http://intearview.com/post/2012/03/01/Jims-experience.aspx

